import java.util.ArrayList;
import java.util.List;

/**
 * Created by Awdul on 16.03.2017.
 */
public class Hand {
    private ArrayList<Card> Cards;



    void AddCard(Deck WhichDeck)
    {
        Cards.add(WhichDeck.TakeOneCard());
        //System.out.println("dodano karte o wartosci:");
    }

    int GiveValueOfHand()
    {
        int sum=0;
        for(Card i : Cards)
        {
            sum=sum+i.ValueOfCard();
        }
        if(TryToAceIntoOne()==true) sum=sum-10;
        System.out.println("suma: "+sum);
        return sum;
    }

    boolean TryToAceIntoOne()
    {
        for(Card i: Cards)
        {
            if((i.ValueOfCard())==11) return true;
        }
        return false;
    }

    public Hand()
    {
        Cards = new ArrayList<Card>();
    }

    void ShowCardsInHand( boolean Croupier)
    {
        int i=0;
        if(Croupier==true){
            System.out.print("Karty krupiera:\n[]");
            i++;
        }
        else {
            System.out.print("Karty gracza:");
        }
        for(i=0; i<Cards.size();i++)
        {
            Cards.get(i).ShowCard();
        }
    }

    void DestoryHand()
    {
        Cards.clear();
    }

    int HowManyCards()
    {
        return Cards.size();
    }
}
