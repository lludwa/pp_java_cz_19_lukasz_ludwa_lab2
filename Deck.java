import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Awdul on 18.03.2017.
 */
public class Deck {
    private ArrayList<Card> MyDeck;


    void CreateDeck()
    {
        for(int i=2; i<15; i++)
        {
            for(int j=0; j<4; j++)
            {
                MyDeck.add(new Card(i,j));
            }
        }
    }

    void ShuffleDeck()
    {
       // ArrayList<Card> AncillaryDeck;
        ArrayList AncillaryDeck=new ArrayList<Card>(MyDeck);
        //AncillaryDeck=MyDeck.clone();
        MyDeck.clear();
        Card AncillaryCard;
        int index;
        Random generator = new Random();
        int sizeOfAncillaryDeck=AncillaryDeck.size();
        while(sizeOfAncillaryDeck>0)
        {
            index=generator.nextInt(sizeOfAncillaryDeck);
            MyDeck.add((Card) AncillaryDeck.get(index));
            AncillaryDeck.remove(index);
            sizeOfAncillaryDeck--;
        }
    }

    Card TakeOneCard()
    {
        Card tmp;
        /*if(MyDeck.size()==0) System.out.println("Nie ma kart!!");
        else {
            tmp = MyDeck.get(0);
            MyDeck.remove(0);
            return tmp;
        }
        return tmp;
        */
        tmp = MyDeck.get(0);
        MyDeck.remove(0);
        return tmp;
    }

    void DestroyDeck()
    {
        MyDeck.clear();
    }

    public Deck()
    {
        MyDeck=new ArrayList<Card>();
    }
}
